package entites;

import java.time.LocalDate;
import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Kevin
 */
public class Employe extends Personne{
    private String cnss;
    private LocalDate dateEmbauche;

    public String getCnss() {
        return cnss;
    }

    public void setCnss(String cnss) {
        this.cnss = cnss;
    }

    public LocalDate getDateEmbauche() {
        return dateEmbauche;
    }

    public void setDateEmbauche(LocalDate dateEmbauche) {
        this.dateEmbauche = dateEmbauche;
    }

    public Employe(long id, String nom, String prenom, LocalDate dateNaissance) {
        super(id, nom, prenom, dateNaissance);
    }
    
    public Employe(long id, String nom, String prenom, LocalDate dateNaissance, String cnss, LocalDate dateEmbauche) {
        super(id, nom, prenom, dateNaissance);
        this.cnss = cnss;
        this.dateEmbauche = dateEmbauche;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.cnss);
        hash = 53 * hash + Objects.hashCode(this.dateEmbauche);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employe other = (Employe) obj;
        return Objects.equals(this.cnss, other.cnss);
    }

    @Override
    public String toString() {
        return "Employe{" + "cnss=" + cnss + ", dateEmbauche=" + dateEmbauche + '}';
    }
    
}

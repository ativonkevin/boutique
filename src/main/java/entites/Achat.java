package entites;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Kevin
 */
public class Achat {
    private long id;
    private double remise = 0.0;
    private LocalDate dateAchat;
    private ArrayList<ProduitAchete> achatList;
    
    //Default constructor
    public Achat(){}
    
    //Constructor
    public Achat(long id, double remise, LocalDate dateAchat, ArrayList<ProduitAchete> achatList) {
        this.id = id;
        this.remise = remise;
        this.dateAchat = dateAchat;
        this.achatList = achatList;
    }
    
    //Getter and Setter
    public long getId(){
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getRemise() {
        return remise;
    }

    public void setRemise(double remise) {
        this.remise = remise;
    }

    public LocalDate getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(LocalDate dateAchat) {
        this.dateAchat = dateAchat;
    }

    public ArrayList<ProduitAchete> getAchatList() {
        return achatList;
    }

    public void setAchatList(ArrayList<ProduitAchete> achatList) {
        this.achatList = achatList;
    }
    //Add element to list
    public void addAchatList(ProduitAchete achete){
        this.achatList.add(achete);
    }
    //Remove element to list
    public void removeAchatList(int index){
        this.achatList.remove(index);
    }

    public double getPrixTotal() {
        double price = 0;
        for(ProduitAchete i : achatList){
            price += i.getPrixTotal();
        }
        return price * (1-remise);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 23 * hash + (int) (Double.doubleToLongBits(this.remise) ^ (Double.doubleToLongBits(this.remise) >>> 32));
        hash = 23 * hash + Objects.hashCode(this.dateAchat);
        hash = 23 * hash + Objects.hashCode(this.achatList);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Achat other = (Achat) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Achat{" + "id=" + id + ", remise=" + remise + ", dateAchat=" + dateAchat + ", achatList=" + achatList + '}';
    }
    
}
